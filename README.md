# Welcome Group A AutomaTa_Project

## Task Divide

|       Name       |                      Respone                          |
| ---------------- | ----------------------------------------------------- |
| Mao Sonihour     | a. Design a finite automaton (FA)                     |
| Hem Kimsokonthea | b. Test if a FA is deterministic or non-deterministic |
| Oum Chankreas    | c. Test if a string is accepted by a FA               |
| Cheng Rithya     | d. Construct an equivalent DFA from an NFA            |
| Meng Visal       | e. Minimize a DFA                                     | 
| Korn Sanit       | e. Minimize a DFA                                     |

### How to use

1. Clone or Remote this AutomaTa_Project
2. Create your branch
3. Push into your branch






